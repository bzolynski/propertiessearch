<?php

namespace ArchitectureLogic\Service;

use Exception;

class ApiSoapMockService implements ApiSoapInterface
{
    use HashTrait;

    /**
     * Parameters for a request
     *
     * @var array
     */
    private $params = array();

    /**
     * Cache Service instance
     *
     * @var CacheServiceInterface
     */
    private $cacheService = null;

    /**
     * Is cache enabled for a single request
     *
     * @var boolean
     */
    private $readCache = false;

    public function __construct($apiKey, $apiWsdl, CacheServiceInterface $cacheService = null)
    {
        $this->apiKey = $apiKey;
        $this->apiWsdl = $apiWsdl;
        if (!is_null($cacheService)) {
            $this->cacheService = $cacheService;
        }

        $this->params['api_key'] = $this->apiKey;
    }

    /**
     * Method caller
     *
     * @param string $methodName
     * @param array $params
     * @return mixed
     */
    public function call($methodName, array $params = null)
    {

        //Get result from a cache if exists
        if ($this->isCacheEnabled()) {

            //Prepare hash for a cache
            $this->setCallHash($methodName . (!is_null($params) ? json_encode($params) : ''));

            try {
                if ($this->cacheService->exists($this->getCallHash())) {

                    //Reset status of cache for further requests
                    $this->setCacheEnabled(false);

                    return json_decode($this->cacheService->get($this->getCallHash()), true);
                }
            } catch (Exception $e) {
                //If no cache available, get data directly from a mock
                $this->setCacheEnabled(false);
            }
        }

        if ($methodName === 'areas') {
            $results = array(
                'areas' => array(
                    array(
                        'id' => 1,
                        'name' => 'Tokyo',
                        'properties' => 24
                    ),
                    array(
                        'id' => 3,
                        'name' => 'Warsaw',
                        'properties' => 54
                    ),
                    array(
                        'id' => 8,
                        'name' => 'Ottawa',
                        'properties' => 32
                    )
                )
            );
        } else if ($methodName === 'search_sale') {
            $results = array(
                'results' => array(
                    'ads' => array(
                        array(
                            'ad_id' => 2020202,
                            'daft_url' => 'http://',
                            'full_address' => 'Address 11',
                            'property_type' => 'house',
                            'bedrooms' => 2,
                            'rent' => 1500,
                            'large_thumbnail_url' => '',
                        ),
                        array(
                            'ad_id' => 1010101,
                            'daft_url' => 'http://',
                            'full_address' => 'Address 13',
                            'property_type' => 'house',
                            'bedrooms' => 3,
                            'rent' => 1900,
                            'large_thumbnail_url' => '',
                        )
                    )
                )
            );
        } else {
            $results = array();
        }

        //Put result to cache
        if ($this->isCacheEnabled()) {
            $this->cacheService->set($this->getCallHash(), json_encode($results));

            //Make associative array output for further use in request
            $results = json_decode(json_encode($results), true);

            //Reset status of cache for further requests
            $this->setCacheEnabled(false);
        }

        return $results;
    }

    /**
     * Checks if cache is enabled for a single request
     *
     * @return boolean
     */
    public function isCacheEnabled()
    {
        return $this->readCache === true;
    }

    /**
     * Enable read cache mode for a request
     *
     * @return $this
     */
    public function readCache()
    {
        $this->setCacheEnabled(true);
        return $this;
    }

    public function setCacheEnabled($state)
    {
        $this->readCache = $state;
    }
}
