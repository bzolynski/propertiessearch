<?php

namespace ArchitectureLogic\Service;

use Symfony\Component\Yaml\Parser;
use Exception;

abstract class AbstractRouteService
{

    /**
     * List of allowed HTTP methods
     *
     * @var array
     */
    private $methodsAllowed = array('get', 'post', 'put', 'delete', 'patch', 'head');

    /**
     * List of routes
     *
     * @var array
     */
    protected $routes = array();

    /**
     * Parse configuration in YAML format
     *
     * @param $routeConfigFile
     * @throws Exception
     */
    public function __construct($routeConfigFile)
    {
        if (is_file($routeConfigFile)) {
            $yamlParser = new Parser();
            $data = $yamlParser->parse(file_get_contents($routeConfigFile));

            $this->routes = $this->retrieveRoutes($data);

            return;
        }

        throw new Exception('Route configuration file "' . $routeConfigFile .  '" not found error.');
    }

    /**
     * Returns all routes
     *
     * @return array
     */
    public function getAll()
    {
        return $this->routes;
    }

    /**
     * Returns data for specific route URL
     *
     * @param string $routeUrl
     * @throws Exception
     */
    public function get($routeUrl)
    {
        if (isset($this->routes[$routeUrl])) {
            return $this->routes[$routeUrl];
        } else {
            throw new Exception('Route with URL: "' . $routeUrl . '" not defined error.');
        }
    }

    /**
     * Retrieve routes from configuration array
     *
     * @param array $data
     * @return array
     * @throws Exception
     */
    protected function retrieveRoutes(array $data)
    {
        $routes = array();

        foreach($data as $routeName=>$route) {
            if (isset($route['path']) && $route['path']) {
                $path = $route['path'];
            } else {
                throw new Exception('Path not found for "' . $routeName . '" route.');
            }

            if (isset($route['action']) && $route['action']) {
                if (!strpos($route['action'], ':')) {
                    throw new Exception('Controller and function name must be separated by semicolon in action for "' . $routeName . '".');
                }
                $action = $route['action'];
            } else {
                throw new Exception('Action not found for "' . $routeName . '" route.');
            }

            if (isset($route['method']) && $route['method']) {
                if (!in_array(strtolower($route['method']), $this->methodsAllowed)) {
                    throw new Exception('Method not allowed for "' . $routeName . '" route.');
                }
                $method = strtolower($route['method']);
            } else {
                throw new Exception('Method not found for "' . $routeName . '" route.');
            }

            $routes[$path][$method] = $action;
        }

        return $routes;
    }
}
