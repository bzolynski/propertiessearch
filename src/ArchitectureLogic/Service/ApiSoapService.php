<?php

namespace ArchitectureLogic\Service;

use SoapClient;
use Exception;

class ApiSoapService implements ApiSoapInterface
{
    use HashTrait;

    /**
     * @var SoapClient
     */
    private $client = null;

    /**
     * Parameters for a request
     *
     * @var array
     */
    private $params = array();

    /**
     * Cache Service instance
     *
     * @var CacheServiceInterface
     */
    private $cacheService = null;

    /**
     * Is cache enabled for a single request
     *
     * @var boolean
     */
    private $readCache = false;

    private $apiKey = null;

    private $apiWsdl = null;

    /**
     * @param string $apiKey
     * @param string $apiWsdl
     * @param CacheServiceInterface $cacheService
     */
    public function __construct($apiKey, $apiWsdl, CacheServiceInterface $cacheService = null)
    {
        $this->apiKey = $apiKey;
        $this->apiWsdl = $apiWsdl;
        if (!is_null($cacheService)) {
            $this->cacheService = $cacheService;
        }

        $this->params['api_key'] = $this->apiKey;
        try {
            $this->client = new SoapClient($this->apiWsdl);
        } catch (Exception $e) {
            //Cannot load external entity exception
            die($e->getMessage());
        }
    }

    /**
     * Method caller
     *
     * @param string $methodName
     * @param array $params
     * @return mixed
     */
    public function call($methodName, array $params = null)
    {
        //Get result from a cache if exists
        if ($this->isCacheEnabled()) {

            //Prepare hash for a cache
            $this->setCallHash($methodName . (!is_null($params) ? json_encode($params) : ''));

            try {
                if ($this->cacheService->exists($this->getCallHash())) {

                    //Reset status of cache for further requests
                    $this->setCacheEnabled(false);

                    return json_decode($this->cacheService->get($this->getCallHash()), true);
                }
            } catch (Exception $e) {
                //If no cache available, get data directly from API
                $this->setCacheEnabled(false);
            }
        }

        $results = (array)$this->client->$methodName(
            !is_null($params) ? array_merge($params, $this->params) : $this->params
        );

        //Put result to cache
        if ($this->isCacheEnabled()) {
            $this->cacheService->set($this->getCallHash(), json_encode($results));

            //Make associative array output for further use in request
            $results = json_decode(json_encode($results), true);

            //Reset status of cache for further requests
            $this->setCacheEnabled(false);
        }

        return $results;
    }

    /**
     * Checks if cache is enabled for a single request
     *
     * @return boolean
     */
    public function isCacheEnabled()
    {
        return $this->readCache === true;
    }

    /**
     * Enable read cache mode for a request
     *
     * @return $this
     */
    public function readCache()
    {
        $this->setCacheEnabled(true);
        return $this;
    }

    public function setCacheEnabled($state)
    {
        $this->readCache = $state;
    }
}
