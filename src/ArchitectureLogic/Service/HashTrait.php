<?php

namespace ArchitectureLogic\Service;

trait HashTrait
{
    /**
     * Hash for a single request, used as a key for cache
     *
     * @var string
     */
    private $callHash = null;

    private function getCallHash()
    {
        return $this->callHash;
    }

    private function setCallHash($callHash)
    {
        $this->callHash = hash('sha256', $callHash);
    }
}
