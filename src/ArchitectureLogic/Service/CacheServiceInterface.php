<?php

namespace ArchitectureLogic\Service;

interface CacheServiceInterface
{
    public function __construct(array $settings = null);

    public function get($key);

    public function set($key, $value, $timeout = 0);

    public function exists($key);
}
