<?php

namespace ArchitectureLogic\Service;

use Redis;

class RedisService implements CacheServiceInterface
{

    private $redisInstance = null;

    /**
     * Instantiate Redis cache
     *
     * @param array $settings
     */
    public function __construct(array $settings = null)
    {
        if (class_exists('Redis')) {
            $this->redisInstance = new Redis();
            $this->redisInstance->connect($settings['host'], $settings['port']);
        }
    }

    public function get($key)
    {
        if (!$this->redisInstance instanceof RedisService) {
            return null;
        }
        return $this->redisInstance->get($key);
    }

    public function set($key, $value, $timeout = 0)
    {
        if (!$this->redisInstance instanceof RedisService) {
            return;
        }
        $this->redisInstance->set($key, $value, $timeout);
    }

    public function exists($key)
    {
        if (!$this->redisInstance instanceof RedisService) {
            return false;
        }
        return $this->redisInstance->exists($key);
    }
}
