<?php

namespace ArchitectureLogic\Service;

interface ApiSoapInterface
{

    /**
     * @param string $apiKey
     * @param string $apiWsdl
     * @param CacheServiceInterface $cacheService
     */
    public function __construct($apiKey, $apiWsdl, CacheServiceInterface $cacheService = null);

    /**
     * Method caller
     *
     * @param string $methodName
     * @param array $params
     * @return mixed
     */
    public function call($methodName, array $params = null);

    /**
     * Checks if cache is enabled for a single request
     *
     * @return boolean
     */
    public function isCacheEnabled();

    /**
     * Enable read cache mode for a request
     *
     * @return $this
     */
    public function readCache();

    public function setCacheEnabled($state);
}
