<?php

namespace ArchitectureLogic\Service;

use Symfony\Component\Yaml\Parser;
use Exception;

class YamlRouteService extends AbstractRouteService
{

    /**
     * Parse configuration in YAML format
     *
     * @param string $routeConfigFile
     * @throws Exception
     */
    public function __construct($routeConfigFile)
    {
        if (is_file($routeConfigFile)) {
            $yamlParser = new Parser();
            $data = $yamlParser->parse(file_get_contents($routeConfigFile));

            $this->routes = $this->retrieveRoutes($data);

            return;
        }

        throw new Exception('Route configuration file "' . $routeConfigFile .  '" not found error.');
    }

}
