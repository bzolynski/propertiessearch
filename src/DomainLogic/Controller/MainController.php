<?php

namespace DomainLogic\Controller;

use ArchitectureLogic\Controller\DefaultController;
use ArchitectureLogic\Service\RedisService;
use ArchitectureLogic\Service\ApiSoapInterface;

use DomainLogic\Service\KeywordsParserService;
use DomainLogic\Dictionary\AreaDictionary;
use DomainLogic\Dictionary\MethodTypeDictionary;
use DomainLogic\Dictionary\PropertyTypeDictionary;
use DomainLogic\Dictionary\BedroomDictionary;
use DomainLogic\Dictionary\PriceDictionary;

class MainController extends DefaultController
{

    const DEFAULT_API_SOAP_SERVICE_CLASS = 'ArchitectureLogic\Service\ApiSoapService';

    /**
     * @var ApiSoapInterface $api
     */
    protected $api;

    public function indexAction()
    {
        $this->render('index.twig.php');
    }

    public function searchAction()
    {
        $keywords = $this->app->request->post('keywords');

        if (strlen($keywords) < 5) {
            print '<div class="alert alert-warning col-md-10">Keywords must be at least 5 characters long.</div>';
            return;
        }

        $api = $this->getApiInstance();
        $this->setApi($api);

        //Get areas list
        $areasParams = array(
            'area_type' => 'area'
        );
        $areasList = $this->api->readCache()->call('areas', $areasParams);

        $dictionaries = array(
            new MethodTypeDictionary(),
            new AreaDictionary($areasList['areas']),
            new PropertyTypeDictionary(),
            new BedroomDictionary(),
            new PriceDictionary()
        );

        //Instantiate KeywordsParserService
        $keywordsParser = new KeywordsParserService($keywords, $dictionaries);

        $resolvedData = $keywordsParser->parse();

        $params = array(
            'query' => array()
        );

        if (isset($resolvedData['method_type'])) {
            $callMethod = $resolvedData['method_type'];
        } else {
            $callMethod = 'search_sale';
        }

        if (isset($resolvedData['bedrooms'])) {
            $params['query']['bedrooms'] = $resolvedData['bedrooms'];
        }

        if (isset($resolvedData['property_type'])) {
            $params['query']['property_type'] = $resolvedData['property_type'];
        }

        if (isset($resolvedData['max_price'])) {
            $params['query']['max_price'] = $resolvedData['max_price'];
        }

        //Note: This parameter is not working in API...
        /*
        if (isset($resolvedData['areas'])) {
            $params['query']['cities'] = array_keys($resolvedData['areas']);
        }
        */

        $results = $this->api->call($callMethod, $params);

        $results = json_decode(json_encode($results['results']), true);

        //Rebuild result array for a single result
        if (isset($results['ads']) && !isset($results['ads'][0])) {
            $results['ads'] = array($results['ads']);
        }
        $this->render('results.twig.php', array(
            'areas' => isset($resolvedData['areas']) ? $resolvedData['areas'] : array(),
            'results' => $results
        ));
    }

    protected function getApiInstance()
    {
        $settings = $this->app->container->get('settings');
        $config = $settings['app.settings'];

        $redis = null;

        if (isset($config['redis']) && is_array($config['redis'])) {
            $redisSettings = array(
                'host' => isset($config['redis']['host']) ? $config['redis']['host'] : null,
                'port' => isset($config['redis']['port']) ? $config['redis']['port'] : null
            );

            $redis = new RedisService($redisSettings);
        }

        if (isset($config['api_soap_service_class']) && $config['api_soap_service_class']) {
            $apiSoapServiceClass = $config['api_soap_service_class'];
        } else {
            $apiSoapServiceClass = self::DEFAULT_API_SOAP_SERVICE_CLASS;
        }

        return new $apiSoapServiceClass(
            $config['api_key'],
            $config['api_wsdl'],
            $redis
        );
    }

    protected function setApi(ApiSoapInterface $apiSoapService)
    {
        $this->api = $apiSoapService;
    }
}
