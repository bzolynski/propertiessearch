<?php

namespace DomainLogic\Service;

class KeywordsParserService
{
    private $keywords;

    private $dictionaries = array();

    private $resolvedData = array();

    public function __construct($keywords, array $dictionaries)
    {
        $this->keywords = $keywords;
        $this->dictionaries = $dictionaries;
    }

    /**
     * Parse all dictionaries
     *
     * @return array
     */
    public function parse()
    {
        if (!$this->keywords) {
            return array();
        }

        foreach($this->dictionaries as $dictionary) {
            $dictionary->parse($this->keywords);
            $this->keywords = $dictionary->getKeywords();
            $this->resolvedData[$dictionary->getName()] = $dictionary->getResult();
        }

        return $this->resolvedData;

    }

}
