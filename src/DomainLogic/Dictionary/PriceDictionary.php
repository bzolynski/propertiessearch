<?php

namespace DomainLogic\Dictionary;

class PriceDictionary extends AbstractDictionary implements DictionaryInterface
{

    public function getItems()
    {
        return array(
            array('/\b(\d{2,13})\b/i')
        );
    }

    /**
     * Search for max price in keywords string
     *
     * @param string $keywords
     * @return $this
     */
    public function parse($keywords)
    {
        parent::parse($keywords);

        foreach($this->getItems() as $patterns) {

            foreach($patterns as $pattern) {
                preg_match($pattern, $this->keywords, $result);
                if (!empty($result)) {
                    $this->result = $result[1];
                    return $this;
                }
            }
        }

        return $this;

    }

    public function getName()
    {
        return 'max_price';
    }

}
