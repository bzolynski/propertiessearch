<?php

namespace DomainLogic\Dictionary;

interface DictionaryInterface
{

    public function parse($keywords);

    public function getName();

    public function getKeywords();

    public function getResult();

}
