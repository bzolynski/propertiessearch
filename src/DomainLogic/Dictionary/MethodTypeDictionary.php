<?php

namespace DomainLogic\Dictionary;

class MethodTypeDictionary extends AbstractDictionary implements DictionaryInterface
{

    /**
     * List of method types
     * Note: Hierarchy is crucial as the first matched method type will be used
     *
     * @return array
     */
    protected function getItems()
    {
        return array(
            'search_sharing' => array('/shar/i'),
            'search_sale' => array('/sale/i'),
            'search_rental' => array('/rent/i'),
            'search_commercial' => array('/commerc/i'),
            'search_new_development' => array('/devel/i'),
            'search_shortterm' => array('/short/i'),
            'search_parking' => array('/parki/i')
        );
    }

    public function getName()
    {
        return 'method_type';
    }

    /**
     * Search for method type from keywords string
     *
     * @param string $keywords
     * @return $this
     */
    public function parse($keywords)
    {
        parent::parse($keywords);

        foreach($this->getItems() as $searchMethod => $patterns) {

            foreach($patterns as $pattern) {
                preg_match($pattern, $this->keywords, $result);
                if (!empty($result)) {
                    $this->result = $searchMethod;
                    return $this;
                }
            }
        }

        return $this;
    }
}
