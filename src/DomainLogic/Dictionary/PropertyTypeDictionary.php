<?php

namespace DomainLogic\Dictionary;

class PropertyTypeDictionary extends AbstractDictionary implements DictionaryInterface
{

    public function getItems()
    {
        return array(
            'house' => array('/house/i'),
            'apartment' => array('/apartm/i'),
            'duplex' => array('/duplex/i'),
            'bungalow' => array('/bungalow/i'),
            'site' => array('/site/i'),
            'studio' => array('/studio/i')
        );
    }

    /**
     * Search for a property type in keywords string
     *
     * @param string $keywords
     * @return $this
     */
    public function parse($keywords)
    {
        parent::parse($keywords);

        foreach($this->getItems() as $propertyType => $patterns) {

            foreach($patterns as $pattern) {
                preg_match($pattern, $this->keywords, $result);
                if (!empty($result)) {
                    $this->result = $propertyType;
                    return $this;
                }
            }
        }

        return $this;

    }

    public function getName()
    {
        return 'property_type';
    }

}