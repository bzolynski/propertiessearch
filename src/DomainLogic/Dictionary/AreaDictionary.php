<?php

namespace DomainLogic\Dictionary;

class AreaDictionary extends AbstractDictionary implements DictionaryInterface
{

    private $areasList = array();

    public function __construct(array $areasList)
    {
        if (!empty($areasList)) {
            foreach ($areasList as $area) {

                //Patch for all Dublin zone names
                if (preg_match('/^dublin\s\d{1}$/i', $area['name'])) {
                    $area['name'] = 'Dublin';
                }

                $this->areasList[$area['id']] = $area['name'];
            }
        }
    }

    public function getItems()
    {
        return $this->areasList;
    }

    /**
     * Search for area name in keywords string
     *
     * @param string $keywords
     * @return $this
     */
    public function parse($keywords)
    {
        parent::parse($keywords);

        $areas = array();
        foreach($this->getItems() as $areaId => $areaName) {

            preg_match('/\b(' . $areaName . '*)\b/i', $this->keywords, $result);

            if (!empty($result)) {
                $areas[$areaId] = $areaName;
            }
        }

        //Best match search by calculate the similarity
        $lastSimilarity = 0;
        $mostSimilarAreaName = null;
        if (!empty($areas)) {
            foreach($areas as $areaId => $areaName) {

                similar_text(strtolower($areaName), strtolower($this->keywords), $similarity);

                if ($similarity > $lastSimilarity) {
                    $lastSimilarity = $similarity;
                    $mostSimilarAreaName = $areaName;
                }
            }

            if (!is_null($mostSimilarAreaName)) {
                foreach($areas as $areaId => $areaName) {
                    if (!strcmp($areaName, $mostSimilarAreaName)) {
                        $this->result[$areaId] = $areaName;
                    }
                }
            } else {
                $this->result = $areas;
            }

        }

        return $this;

    }

    public function getName()
    {
        return 'areas';
    }
}
