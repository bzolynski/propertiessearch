<?php

namespace DomainLogic\Dictionary;

class AbstractDictionary implements DictionaryInterface
{
    protected $keywords;

    protected $result;

    /**
     * Method to parse keywords string
     *
     * @param string $keywords
     */
    public function parse($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * Method to get keywords
     * Note that's possible to modify a keyword while executing dictionaries
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Get name of dictionary as a key
     *
     * @return string
     */
    public function getName()
    {
        return null;
    }

    /**
     * Get result of dictionary parsing
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
}
