<?php

namespace DomainLogic\Dictionary;

class BedroomDictionary extends AbstractDictionary implements DictionaryInterface
{

    public function getItems()
    {
        return array(
            array('/(\d{1})\sbedroom/i'),
            array('/(\d{1})\sbed/i')
        );
    }

    /**
     * Search for bedroom string in keywords string
     *
     * @param string $keywords
     * @return $this
     */
    public function parse($keywords)
    {
        parent::parse($keywords);

        foreach($this->getItems() as $patterns) {

            foreach($patterns as $pattern) {
                preg_match($pattern, $this->keywords, $result);
                if (!empty($result)) {
                    $this->result = $result[1];
                    return $this;
                }
            }
        }

        return $this;

    }

    public function getName()
    {
        return 'bedrooms';
    }

}
