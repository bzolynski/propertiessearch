<?php

use SlimController\Slim;
use ArchitectureLogic\Service\YamlRouteService;
use Symfony\Component\Yaml\Parser;

require 'vendor/autoload.php';

$app = new Slim(array(
    'templates.path'             => './templates',
    'controller.class_prefix'    => '\\DomainLogic\\Controller',
    'controller.method_suffix'   => 'Action',
    'controller.template_suffix' => 'php',
    'view' => new \Slim\Views\Twig()
));

//Set routes from YAML configuration
try {
    $routeService = new YamlRouteService('config/routing.yml');
    $routes = $routeService->getAll();
    $app->addRoutes($routes);
} catch (Exception $e) {
    die($e->getMessage());
}

//Set application settings
$yamlParser = new Parser();
$appConfig = $yamlParser->parse(file_get_contents('config/app.yml'));
$app->config('app.settings', $appConfig);

//Run application
$app->run();
