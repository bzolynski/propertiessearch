/**
 * Events
 */
$('.form .search').click(function() {
    submit();
});

$('.form').bind("keypress", function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        $('.form input[name="keywords"]').blur();
        submit();
    }
});

/**
 * Search form action
 */
var submit = function() {
    messageBox().show('Please wait...', 'alert-info');

    $.ajax({
        url: "/search",
        method: "POST",
        context: document.body,
        data: { keywords: $('.form input[name="keywords"]').val() }
    }).done(function(response) {
        messageBox().hide();
        $('.results').html(response);
    });
};

/**
 * Method to message alerts control
 * @returns {{show: Function, hide: Function}}
 */
var messageBox = function() {
    var show = function(message, type) {
        $('.results').html('');
        $('.messageBox')
            .addClass(type)
            .html(message)
            .show();
        },
        hide = function() {
            $('.messageBox')
                .hide();
        };

    return {
        show: show,
        hide: hide
    }
};
