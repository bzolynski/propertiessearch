{% extends "base.twig.php" %}

{% block title %}Search{% endblock %}

{% block content %}
    <br><br>
    <div class="container-fluid">
        <div class="col-sm-offset-1 col-md-10">
            <form method="POST" action="/search" class="form">
                <div class="form-group col-md-10">
                    <input type="text" name="keywords" placeholder="Free text properties search" value="{{ keywords }}" class="form-control">
                </div>
                <button type="button" class="search btn btn-default">Search</button>
            </form>
        </div>
        <div class="col-sm-offset-2 col-md-7">
            <div class="alert messageBox"></div>
        </div>
        <div class="col-sm-offset-1 col-md-10">
            <div class="results"></div>
        </div>
    </div>


{% endblock %}
