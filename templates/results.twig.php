Search sentence: {{ results.search_sentence }}<br><br>
Areas:
{% for id, area in areas %}
    {% if loop.index > 1 %}, {% endif %}
    {{ area }} [id: {{ id }}]
    {% if loop.last %}.{% endif %}
{% endfor %}
{% if not areas %}
    No areas found in keywords.
{% endif %}
<br><br>

{% for result in results.ads %}

    <div class="row u-margin-bottom-sm">
        <div class="col-sm-3">
            <a href="{{ result.daft_url }}" target="_blank">
                <img src="{{ result.large_thumbnail_url }}" alt="" title="">
            </a>
        </div>
        <div class="col-sm-7">
            <h2>{{ result.full_address }}</h2>
            <b>Property type:</b> {{ result.property_type }}<br>
            <b>Bedrooms:</b> {{ result.bedrooms }}<br>
            {% if result.rent %}
            <b>Rent:</b> &euro;{{ result.rent }}
            {% else %}
            <b>Price:</b> &euro;{{ result.price }}
            {% endif %}
        </div>
    </div>

{% endfor %}

{% if not results.ads %}
    <div class="alert alert-warning">Sorry, no results found.</div>
{% endif %}
