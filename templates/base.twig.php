<!DOCTYPE html>
<html>
<head>
    {% block head %}
    <link rel="stylesheet" href="/styles/style.css" />
    <link rel="stylesheet" href="/vendor/twitter/bootstrap/dist/css/bootstrap.min.css" />
    <title>{% block title %}{% endblock %} - Demo</title>
    {% endblock %}
</head>
<body>
<div id="content">{% block content %}{% endblock %}</div>
<div id="footer">
    {% block footer %}

    {% endblock %}
</div>
{% block javascripts %}
    <script type="text/javascript" src="/scripts/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/scripts/apiRest.js"></script>
{% endblock %}
</body>
</html>
