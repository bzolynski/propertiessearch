# Properties Search

**Technologies used:**
PHP, Slim framework, Redis cache, Yaml reader, Twig templates, Twitter Bootstrap, jQuery.

**Features:**  

* Any cache which implements *CacheServiceInterface* can be used to cached some of API requests,  
  you can also do not use cache at all, Redis has been used for caching areas list in this example  
* In dictionaries (DomainLogic/Dictionary namespace) you can define rules to parse keywords string,  
  all dictionaries are loose coupled so it's easy to use some of them for particular queries  
+ Available dictionaries for parsing search query:  
    * AreaDictionary: e.g. Clady Milltown, Milltown Malbay, Milltown, etc.  
    * BedroomDictionary: e.g. 2 beds, 3 bedrooms  
    * MethodTypeDictionary: sale, rent, etc.  
    * PriceDictionary: max price value, e.g. 195000  
    * PropertyTypeDictionary: e.g. apartment, house, bungalow, etc.  
* Configuration keeped in yaml format, just copy config/app.yml.dist to app.yml and fill with  
  your credentials  
* Twig templates to keep it clean  
* Response is retrieved via jQuery AJAX so whole web page is not reloading on each search request  
* ApiSoapService can be mocked by ApiSoapMockService or any other service which implements ApiSoapInterface (Strategy Pattern used here)
